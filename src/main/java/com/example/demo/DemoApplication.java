package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {
	double variable_1;
	int variable_2;
	int variable_3;
	public int getVariable_2() {
		return variable_3 ;
	}
	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}
	@RequestMapping("/endpoint_1")
	public void endpoint_1() {
		throw new RuntimeException("Not working");
	}
	@RequestMapping("/endpoint_2")
	public void endpoint_2() {
		throw new RuntimeException("Not working");
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}